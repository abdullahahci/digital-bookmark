package com.ahci.digitalbookmark.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.ahci.digitalbookmark.R;
import com.ahci.digitalbookmark.commons.ImageUtil;
import com.ahci.digitalbookmark.model.Book;

import java.util.ArrayList;

/**
 * Created by Aahci on 11.5.2016.
 */
public class BookAdapter extends ArrayAdapter<Book> {
    ArrayList<Book> bookList;

    public BookAdapter(Context context, ArrayList<Book> bookList) {
        super(context, 0);
        this.bookList = bookList;
    }

    @Override
    public int getCount() {
        return bookList.size();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.list_item_book, parent, false);
        }

        try {
            Book book = bookList.get(position);


            ImageView bookImage = (ImageView)convertView.findViewById(R.id.book_image);
//            File imgFile = new  File("/sdcard/Images/test_image.jpg");

            try {

                if(book.getImagePath() != null && !"".equalsIgnoreCase(book.getImagePath()))
                    bookImage.setImageBitmap(new ImageUtil(getContext()).setFileName(book.getImagePath()).load());
                else
                    bookImage.setImageResource(R.drawable.book_default);

            }
            catch (Exception e){
                bookImage.setImageResource(R.drawable.book_default);
            }

//            if(imgFile.exists()){
//                Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
//                bookImage.setImageBitmap(myBitmap);
//            }
//            else {
//                bookImage.setImageResource(R.drawable.book_default);
//            }

            TextView bookName = (TextView)convertView.findViewById(R.id.book_name);
            bookName.setText(book.getTitle());

            TextView currentPage = (TextView)convertView.findViewById(R.id.current_page);
            String text = "On the page: #" + String.valueOf(book.getCurrentPage());
            currentPage.setText(text);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return convertView;
    }
}
