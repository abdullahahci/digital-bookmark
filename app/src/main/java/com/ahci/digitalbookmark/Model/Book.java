package com.ahci.digitalbookmark.model;

import com.ahci.digitalbookmark.commons.Util;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by Aahci on 11.5.2016.
 */
public class Book implements Serializable{

    public static enum BookStatus {
        READING (1),
        WILL_READ (2),
        HAVE_READ (3);

        private final int value;

        private BookStatus(int value){
            this.value = value;
        }

        public int getValue(){
            return value;
        }

        public static BookStatus getValue(String value, String[]statuses){
            if(value.equalsIgnoreCase(statuses[0]))
                return READING;
            else if(value.equalsIgnoreCase(statuses[1]))
                return WILL_READ;
            else if(value.equalsIgnoreCase(statuses[2]))
                return HAVE_READ;
            else
                return WILL_READ;
        }

        public static BookStatus valueOf(int value){
            switch (value){
                case 1 : return READING;
                case 2 : return WILL_READ;
                case 3 : return HAVE_READ;
                default : return WILL_READ;
            }
        }
    }

    private int id;
    private String title;
    private String author;
    private int pageCount = Util.DEFFAULT_PAGE_COUNT;
    private int currentPage = 1;
    private BookStatus currentStatus = BookStatus.WILL_READ;
    private boolean starred = false;

    private long dateAdded = new Date().getTime();
    private long dateStarted;
    private long dateFinished;

    private String imagePath;

    public Book() {

    }

    public Book(String title) {
        this.title = title;
        this.author = "";
    }

    public Book(String title, String author, int pageCount) {
        this.title = title;
        this.pageCount = pageCount;
        this.author = author;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getPageCount() {
        return pageCount;
    }

    public void setPageCount(int pageCount) {
        this.pageCount = pageCount;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public int getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(int currentPage) {
        this.currentPage = currentPage;
    }

    public BookStatus getCurrentStatus() {
        return currentStatus;
    }

    public void setCurrentStatus(BookStatus currentStatus) {
        this.currentStatus = currentStatus;
    }

    public boolean isStarred() {
        return starred;
    }

    public void setStarred(boolean starred) {
        this.starred = starred;
    }

    public long getDateAdded() {
        return dateAdded;
    }

    public void setDateAdded(long dateAdded) {
        this.dateAdded = dateAdded;
    }

    public long getDateStarted() {
        return dateStarted;
    }

    public void setDateStarted(long dateStarted) {
        this.dateStarted = dateStarted;
    }

    public long getDateFinished() {
        return dateFinished;
    }

    public void setDateFinished(long dateFinished) {
        this.dateFinished = dateFinished;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }
}
