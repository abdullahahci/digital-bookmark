package com.ahci.digitalbookmark.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.ahci.digitalbookmark.model.Book;

import java.util.ArrayList;

/**
 * Created by Aahci on 11.5.2016.
 */
public class DBUtil extends SQLiteOpenHelper{
    // Database Version
    private static final int DATABASE_VERSION = 1;
    // Database Name
    private static final String DATABASE_NAME = "BookDB";

    private static final String TBL_BOOKS="books";

    private static final String KEY_ID = "id";
    private static final String KEY_TITLE = "title";
    private static final String KEY_AUTHOR = "author";
    private static final String KEY_PAGE_COUNT = "pageCount";
    private static final String KEY_CURRENT_PAGE = "currentPage";
    private static final String KEY_STATUS = "status";
    private static final String KEY_STARRED = "starred";
    private static final String KEY_DATE_ADDED = "dateAdded";
    private static final String KEY_DATE_STARTED = "dateStarted";
    private static final String KEY_DATE_FINISHED = "dateFinished";
    private static final String KEY_IMAGE_PATH = "imagePath";

    private static final String[] COLUMNS = {KEY_ID, KEY_TITLE, KEY_AUTHOR, KEY_PAGE_COUNT, KEY_CURRENT_PAGE,
            KEY_STATUS, KEY_STARRED, KEY_DATE_ADDED, KEY_DATE_STARTED, KEY_DATE_FINISHED, KEY_IMAGE_PATH};

    private static final String SELECT_FIELDS = KEY_ID + ", " + KEY_TITLE + ", " + KEY_AUTHOR + ", " + KEY_PAGE_COUNT + ", " + KEY_CURRENT_PAGE + ", " +
    KEY_STATUS + /*", " + KEY_STARRED +*/ ", " + KEY_DATE_ADDED + ", " + KEY_DATE_STARTED + ", " + KEY_DATE_FINISHED + ", " + KEY_IMAGE_PATH;
    public void addBook(Book book){
        SQLiteDatabase db = this.getWritableDatabase();

        // 2. create ContentValues to add key "column"/value
        ContentValues values = new ContentValues();
        values.put(KEY_TITLE, book.getTitle()); // get title
        values.put(KEY_AUTHOR, book.getAuthor()); // get author
        values.put(KEY_PAGE_COUNT, book.getPageCount()); // get author
        values.put(KEY_CURRENT_PAGE, book.getCurrentPage()); // get author
        values.put(KEY_STATUS, book.getCurrentStatus().getValue()); // get author
        values.put(KEY_STARRED, book.isStarred()); // get author
        values.put(KEY_DATE_ADDED, book.getDateAdded()); // get author
        values.put(KEY_DATE_STARTED, book.getDateStarted()); // get author
        values.put(KEY_DATE_FINISHED, book.getDateFinished()); // get author
        values.put(KEY_IMAGE_PATH, book.getImagePath()); // get author

        db.insert(TBL_BOOKS, null, values);

        db.close();

    }

    public Book getBook(int id){

        // 1. get reference to readable DB
        SQLiteDatabase db = this.getReadableDatabase();

        // 2. build query
        Cursor cursor =
                db.query(TBL_BOOKS, // a. table
                        COLUMNS, // b. column names
                        " id = ?", // c. selections
                        new String[] { String.valueOf(id) }, // d. selections args
                        null, // e. group by
                        null, // f. having
                        null, // g. order by
                        null); // h. limit

        // 3. if we got results get the first one
        if (cursor != null)
            cursor.moveToFirst();

        // 4. build book object
        Book book = new Book();
        book.setId(Integer.parseInt(cursor.getString(0)));
        book.setTitle(cursor.getString(1));
        book.setAuthor(cursor.getString(2));

        //log
        Log.d("getBook(" + id + ")", book.toString());

        // 5. return book
        return book;
    }

    public ArrayList<Book> getBooksByStatus(int status){
        ArrayList<Book> books = new ArrayList<>();

        // 1. build the query
        String query = "SELECT " + SELECT_FIELDS + " FROM " + TBL_BOOKS + " Where " + KEY_STATUS + " = " + status + " Order By " + KEY_DATE_ADDED + " desc";

        // 2. get reference to writable DB
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        // 3. go over each row, build book and add it to list
        Book book = null;
        if (cursor.moveToFirst()) {
            do {
                book = new Book();
                book.setId(Integer.parseInt(cursor.getString(0)));
                book.setTitle(cursor.getString(1));
                book.setAuthor(cursor.getString(2));
                book.setPageCount(cursor.getInt(3));
                book.setCurrentPage(cursor.getInt(4));
                book.setCurrentStatus(Book.BookStatus.valueOf((cursor.getInt(5))));
                book.setDateAdded(cursor.getLong(6));
                book.setDateStarted(cursor.getLong(7));
                book.setDateFinished(cursor.getLong(8));
                book.setImagePath(cursor.getString(9));

                // Add book to books
                books.add(book);
            } while (cursor.moveToNext());
        }

        Log.d("getBooksByStatus()", books.toString());

        // return books
        return books;
    }
    public ArrayList<Book> getAllBooks() {
        ArrayList<Book> books = new ArrayList<Book>();

        // 1. build the query
        String query = "SELECT " + SELECT_FIELDS + " FROM " + TBL_BOOKS + " Order By " + KEY_DATE_ADDED + " desc";

        // 2. get reference to writable DB
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        // 3. go over each row, build book and add it to list
        Book book = null;
        if (cursor.moveToFirst()) {
            do {
                book = new Book();
                book.setId(Integer.parseInt(cursor.getString(0)));
                book.setTitle(cursor.getString(1));
                book.setAuthor(cursor.getString(2));
                book.setPageCount(cursor.getInt(3));
                book.setCurrentPage(cursor.getInt(4));
                book.setCurrentStatus(Book.BookStatus.valueOf((cursor.getInt(5))));
                book.setDateAdded(cursor.getLong(6));
                book.setDateStarted(cursor.getLong(7));
                book.setDateFinished(cursor.getLong(8));
                book.setImagePath(cursor.getString(9));

                // Add book to books
                books.add(book);
            } while (cursor.moveToNext());
        }

        Log.d("getAllBooks()", books.toString());

        // return books
        return books;
    }


    public int updateBook(Book book) {

        // 1. get reference to writable DB
        SQLiteDatabase db = this.getWritableDatabase();

        // 2. create ContentValues to add key "column"/value
        ContentValues values = new ContentValues();
        values.put("title", book.getTitle()); // get title
        values.put("author", book.getAuthor()); // get author

        // 3. updating row
        int i = db.update(TBL_BOOKS, //table
                values, // column/value
                KEY_ID+" = ?", // selections
                new String[] { String.valueOf(book.getId()) }); //selection args

        // 4. close
        db.close();

        return i;

    }

    public void deleteBook(Book book) {

        // 1. get reference to writable DB
        SQLiteDatabase db = this.getWritableDatabase();


        try {
            // 2. delete
            db.delete(TBL_BOOKS, //table name
                    KEY_ID + " = ?",  // selections
                    new String[]{String.valueOf(book.getId())}); //selections args
        } catch (Exception e) {
            Log.d("deleteBook", "No Book to Delete");
        }

        // 3. close
        db.close();

        //log
        Log.d("deleteBook", book.toString());

    }

    public void deleteAllBooks(){
        SQLiteDatabase db = this.getWritableDatabase();

        // 2. delete
        db.execSQL("delete from "+ TBL_BOOKS);

        // 3. close
        db.close();

        //log
        Log.d("deleteAllBooks", "All deleted");
    }

    public ArrayList<Book> getBookList(){
        return new ArrayList<>();
    }

    public ArrayList<Book> getBookList(Book.BookStatus status){
        return new ArrayList<>();
    }

    public DBUtil(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_BOOK_TABLE = "CREATE TABLE books ( " +
                "id INTEGER PRIMARY KEY AUTOINCREMENT, " +
                "title TEXT, "+
                "author TEXT, "+
                "pageCount INTEGER, "+
                "currentPage INTEGER, "+
                "status INTEGER, "+
                "starred INTEGER, "+
                "dateAdded BIGINT, "+
                "dateStarted BIGINT, "+
                "dateFinished BIGINT, "+
                "imagePath TEXT )";

        // create books table
        db.execSQL(CREATE_BOOK_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS books");
        // create fresh books table
        this.onCreate(db);
    }
}
