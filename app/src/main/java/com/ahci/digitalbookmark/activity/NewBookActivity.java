package com.ahci.digitalbookmark.activity;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.ahci.digitalbookmark.R;
import com.ahci.digitalbookmark.commons.ImageUtil;
import com.ahci.digitalbookmark.db.DBUtil;
import com.ahci.digitalbookmark.model.Book;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * Created by Aahci on 13.5.2016.
 */
public class NewBookActivity extends AppCompatActivity {

    private int PICK_IMAGE_REQUEST = 1;
    private String fileName ="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.acivity_new_book);

        ImageView coverView = (ImageView) findViewById(R.id.cover_image);
        coverView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                // Show only images, no videos or anything else
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                // Always show the chooser (if there are multiple options available)
                startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST);
            }
        });

        final SimpleDateFormat dateFormatter = new SimpleDateFormat("dd-MM-yyyy", Locale.US);
        final Calendar cal = Calendar.getInstance();
        final TextView book_started = (TextView) findViewById(R.id.book_started);
        book_started.setText(cal.get(Calendar.DAY_OF_MONTH) + "-"
                + (cal.get(Calendar.MONTH)+1) + "-" + cal.get(Calendar.YEAR));
        book_started.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new DatePickerDialog(NewBookActivity.this, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        Calendar newDate = Calendar.getInstance();
                        newDate.set(year, monthOfYear, dayOfMonth);
                        book_started.setText(dateFormatter.format(newDate.getTime()));
                    }
                }, cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        final TextView book_ended = (TextView) findViewById(R.id.book_finised);
        book_ended.setText(cal.get(Calendar.DAY_OF_MONTH) + "-"
                + (cal.get(Calendar.MONTH)+1) + "-" + cal.get(Calendar.YEAR));
        book_ended.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new DatePickerDialog(NewBookActivity.this, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        Calendar newDate = Calendar.getInstance();
                        newDate.set(year, monthOfYear, dayOfMonth);
                        book_ended.setText(dateFormatter.format(newDate.getTime()));
                    }
                }, cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH) ).show();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_new_book, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        switch (id) {
            case R.id.save:

                try {
                    Toast.makeText(getApplicationContext(), "About menu item pressed", Toast.LENGTH_SHORT).show();
                    Book book = new Book();
                    book.setImagePath(this.fileName);

                    TextView tv = (TextView)findViewById(R.id.title);
                    book.setTitle(tv.getText()+"");

                    tv = (TextView) findViewById(R.id.author);
                    book.setAuthor(tv.getText() + "");

                    tv = (TextView) findViewById(R.id.page_count);
                    book.setPageCount(Integer.parseInt(tv.getText() + ""));

                    tv = (TextView) findViewById(R.id.current_page);
                    book.setCurrentPage(Integer.parseInt(tv.getText() + ""));

                    Spinner statusSpinner=(Spinner) findViewById(R.id.status);
                    String status = statusSpinner.getSelectedItem().toString();

                    book.setCurrentStatus(Book.BookStatus.getValue(status, getResources().getStringArray(R.array.status_array)));

                    book.setDateAdded(new Date().getTime());

                    tv = (TextView) findViewById(R.id.book_started);
                    String[] startDate = (tv.getText()+"").split("-");
                    Calendar cal = Calendar.getInstance();
                    cal.set(Integer.parseInt(startDate[2]), Integer.parseInt(startDate[1]), Integer.parseInt(startDate[0]));
                    book.setDateStarted(cal.getTimeInMillis());

                    tv = (TextView) findViewById(R.id.book_finised);
                    String[] finishDate = (tv.getText()+"").split("-");
                    cal = Calendar.getInstance();
                    cal.set(Integer.parseInt(finishDate[2]), Integer.parseInt(finishDate[1]), Integer.parseInt(finishDate[0]));
                    book.setDateStarted(cal.getTimeInMillis());

                    new DBUtil(NewBookActivity.this).addBook(book);
                } catch (IllegalArgumentException e) {
                    e.printStackTrace();
                }

                break;
        }
        return super.onOptionsItemSelected(item);
    }
    @Override
     protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null) {

            Uri uri = data.getData();

            CropImage.activity(uri)
                    .setFixAspectRatio(true)
                    .setAspectRatio(2,3)
                    .setGuidelines(CropImageView.Guidelines.ON)
                    .start(this);

//            try {
//                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), uri);
//                // Log.d(TAG, String.valueOf(bitmap));
//
//                ImageView imageView = (ImageView) findViewById(R.id.imageView);
//                imageView.setImageBitmap(bitmap);
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
        }
        else if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                Uri resultUri = result.getUri();
                try {
                    Bitmap croppedImage = MediaStore.Images.Media.getBitmap(getContentResolver(), resultUri);
                    ImageUtil util = new ImageUtil(this);
                    util.save(croppedImage);

                    ImageView coverView = (ImageView) findViewById(R.id.cover_image);
                    coverView.setImageBitmap(croppedImage);
                    this.fileName = util.getFileName();
                } catch (IOException e) {
                    e.printStackTrace();
                }


            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
            }
        }
    }
}
