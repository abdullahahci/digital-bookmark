package com.ahci.digitalbookmark.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.ahci.digitalbookmark.R;
import com.ahci.digitalbookmark.commons.ImageUtil;
import com.ahci.digitalbookmark.model.Book;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by Aahci on 16.5.2016.
 */
public class BookDetailsActivity extends AppCompatActivity{
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_book_details);
//        savedInstanceState.getSerializable()
        Book book = (Book)getIntent().getSerializableExtra("Book");
        ProgressBar progress = (ProgressBar) findViewById(R.id.progress);
        int percent = (100 * book.getCurrentPage()) / book.getPageCount();

        progress.setProgress(percent);

        ImageView iv = (ImageView) findViewById(R.id.book_image);
        ImageUtil util = new ImageUtil(this);
        util.setFileName(book.getImagePath());
        iv.setImageBitmap(util.load());

        TextView title = (TextView) findViewById(R.id.title);
        title.setText(Html.fromHtml("<b>Title : </b>" + book.getTitle()));


        TextView author = (TextView) findViewById(R.id.author);
        author.setText(Html.fromHtml("<b>Author : </b>" + book.getAuthor()));


        TextView startDate = (TextView) findViewById(R.id.start_date);
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy", Locale.US);
        startDate.setText(sdf.format(new Date(book.getDateStarted())));

        TextView percentage = (TextView) findViewById(R.id.percentage);
        percentage.setText(percent+"%");

    }
}
