package com.ahci.digitalbookmark.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.ahci.digitalbookmark.R;
import com.ahci.digitalbookmark.db.DBUtil;
import com.ahci.digitalbookmark.fragment.BookFragment;
import com.ahci.digitalbookmark.model.Book;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    private int PICK_IMAGE_REQUEST = 1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, NewBookActivity.class);
                startActivity(intent);
//                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

//        DBUtil db = new DBUtil(this);
//        // get all books
//        db.getAllBooks();

        // add Books
//        db.addBook(new Book("Android Application Development Cookbook", "Wei Meng Lee", 100));
//        db.addBook(new Book("Android Programming: The Big Nerd Ranch Guide", "Bill Phillips and Brian Hardy", 150));
//        db.addBook(new Book("Learn Android App Development", "Wallace Jackson", 250));

        // get all books
//        List<Book> list = db.getAllBooks();

        // delete one book
//        db.deleteBook(list.get(0));

        // get all books
//        db.getAllBooks();
//        db.deleteAllBooks();
//        db.getAllBooks();
    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        }
        else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        // add
//        FragmentTransaction ft = fm.beginTransaction();
//        ft.add(R.id.your_placehodler, new YourFragment());
//        // alternatively add it with a tag
//        // trx.add(R.id.your_placehodler, new YourFragment(), "detail");
//        ft.commit();

        // remove
//        Fragment fragment = fm.findFragmentById(R.id.your_placehodler);
//        FragmentTransaction ft = fm.beginTransaction();
//        ft.remove(fragment);
//        ft.commit();
        DBUtil db = new DBUtil(this);

        if (id == R.id.reading_now) {
            Bundle bundle = new Bundle();
            bundle.putSerializable("Books", db.getBooksByStatus(Book.BookStatus.READING.getValue()));

            BookFragment fragment = new BookFragment();
            fragment.setActivity(this);
            fragment.setArguments(bundle);

            ft.replace(R.id.fragment_frame, fragment);
            ft.commit();
        } else if (id == R.id.have_read) {
            Bundle bundle = new Bundle();
            bundle.putSerializable("Books", db.getBooksByStatus(Book.BookStatus.HAVE_READ.getValue()));

            BookFragment fragment = new BookFragment();
            fragment.setActivity(this);
            fragment.setArguments(bundle);

            ft.replace(R.id.fragment_frame, fragment);
            ft.commit();
        } else if (id == R.id.will_read) {
            Bundle bundle = new Bundle();
            bundle.putSerializable("Books", db.getBooksByStatus(Book.BookStatus.WILL_READ.getValue()));

            BookFragment fragment = new BookFragment();
            fragment.setActivity(this);
            fragment.setArguments(bundle);

            ft.replace(R.id.fragment_frame, fragment);
            ft.commit();
        } else if (id == R.id.nav_share) {

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
