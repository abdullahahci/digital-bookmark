package com.ahci.digitalbookmark.fragment;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;

import com.ahci.digitalbookmark.R;
import com.ahci.digitalbookmark.activity.BookDetailsActivity;
import com.ahci.digitalbookmark.adapter.BookAdapter;
import com.ahci.digitalbookmark.model.Book;
import com.baoyz.swipemenulistview.SwipeMenu;
import com.baoyz.swipemenulistview.SwipeMenuCreator;
import com.baoyz.swipemenulistview.SwipeMenuItem;
import com.baoyz.swipemenulistview.SwipeMenuListView;

import java.util.ArrayList;

/**
 * Created by Aahci on 11.5.2016.
 */
public class BookFragment extends Fragment {

    Context activity;

    ArrayList<Book> bookList ;

    public BookFragment() {
        setRetainInstance(true);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View fragment = inflater.inflate(R.layout.fragment_book, container, false);

        try {
            Bundle bundle = getArguments();
            bookList = (ArrayList<Book>) bundle.getSerializable("Books");


            final SwipeMenuListView lv = (SwipeMenuListView) fragment.findViewById(R.id.book_list);
            lv.setAdapter(new BookAdapter(activity ,bookList));

            lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                }
            });


            SwipeMenuCreator creator = new SwipeMenuCreator() {
                @Override
                public void create(SwipeMenu menu) {
                    // create "details" item
                    SwipeMenuItem openItem = createItem("Details", R.drawable.ic_info_white);
                    // add to menu
                    menu.addMenuItem(openItem);

                    // create "edit" item
                    SwipeMenuItem editItem = createItem("Edit", R.drawable.ic_edit_white);
                    // add to menu
                    menu.addMenuItem(editItem);
                }
            };

            // set creator
            lv.setMenuCreator(creator);

            lv.setOnMenuItemClickListener(new SwipeMenuListView.OnMenuItemClickListener() {
                @Override
                public boolean onMenuItemClick(int position, SwipeMenu menu, int index) {
                    switch (index) {
                        case 0:
                            // Details
                            Intent intent = new Intent(activity, BookDetailsActivity.class);
                            intent.putExtra("Book", bookList.get(position));
                            activity.startActivity(intent);
//                            Snackbar.make(lv, position + " details clicked", Snackbar.LENGTH_LONG)
//                                    .setAction("Action", null).show();
                            break;
                        case 1:
                            // Edit
                            Snackbar.make(lv, position + " edit clicked", Snackbar.LENGTH_LONG)
                                .setAction("Action", null).show();
                            break;
                    }
                    // false : close the menu; true : not close the menu
                    return false;
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
        return fragment;
    }

    private SwipeMenuItem createItem(String title, int iconId){
        SwipeMenuItem menuItem = new SwipeMenuItem(activity);
        // set item background
        menuItem.setBackground(activity.getResources().getDrawable(R.color.colorAccent));
        // set item width
        menuItem.setWidth(300);
        // set item title
        menuItem.setTitle(title);
        // set item title fontsize
        menuItem.setTitleSize(18);
        // set item title font color
        menuItem.setTitleColor(Color.WHITE);
        // set a icon
        menuItem.setIcon(iconId);

        return menuItem;

    }

    public void setActivity(Context activity){
        this.activity = activity;
    }
}
